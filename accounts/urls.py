from django.conf.urls import url
from rest_framework.authtoken import views as auth_views
from accounts import views


urlpatterns = [
    url(r'^register/', views.UserGenericView.as_view()),
    url(r'^api-token-auth/', auth_views.obtain_auth_token)
]