# EasyTweet API project #
## Why ? ##
Just a sample test task for my possible hiring
## What is this? ##
REST API based on Django Rest Framework
## What it does? ##
EasyTweet allows create small tweets. It supports user registration and token-based authorization.
## Where to find documentation? ##
At Apiary of course :)
http://docs.easytwitt.apiary.io/
