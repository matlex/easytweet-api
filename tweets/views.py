from rest_framework.viewsets import ModelViewSet
from tweets.models import Tweet
from tweets.serializers import TweetSerializer
from rest_framework import permissions
from tweets.permissions import IsOwnerOrReadOnly, IsTweetPrivate
from rest_framework.response import Response
from rest_framework.decorators import list_route


class TweetViewSet(ModelViewSet):
    """
    Performs Tweets manipulating.
    /?mine=true - returns tweets owned by authorized user.
    /tweets/<id>/ - returns particular tweet info by id.
    /list - returns all public tweets if not authorized. otherwise returns all both public and private tweets. >>
            also it can return owner's tweets by parameter mine=true.
    /public - returns only public tweets. if authorized it can return owner's public tweets by parameter mine=true.
    /private - returns only private tweets if authorized. can return owner's private tweets by parameter mine=true.
    /accounts/register - Creates a new user via POST and returns it's token key.
    """
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, IsTweetPrivate)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def list(self, request, *args, **kwargs):
        if not request.user.is_authenticated():  # Show only public tweets if not authorized request.
            tweets = Tweet.objects.filter(public=True).order_by('-id')
        elif request.query_params.get('mine'):
            tweets = Tweet.objects.filter(owner=self.request.user).order_by('-id')
        else:
            tweets = Tweet.objects.all().order_by('-id')

        page = self.paginate_queryset(tweets)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(tweets, many=True)
        return Response(serializer.data)

    @list_route()
    def public(self, request, *args, **kwargs):
        if request.user.is_authenticated():  # If authorized user.
            if request.query_params.get('mine'):
                tweets = Tweet.objects.filter(owner=self.request.user, public=True).order_by('-id')
            else:
                tweets = Tweet.objects.filter(public=True).order_by('-id')
        else:
            tweets = Tweet.objects.filter(public=True).order_by('-id')

        page = self.paginate_queryset(tweets)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(tweets, many=True)
        return Response(serializer.data)

    @list_route(permission_classes=[permissions.IsAuthenticated])
    def private(self, request, *args, **kwargs):
        if request.query_params.get('mine'):
            tweets = Tweet.objects.filter(owner=self.request.user, public=False).order_by('-id')
        else:
            tweets = Tweet.objects.filter(public=False).order_by('-id')

        page = self.paginate_queryset(tweets)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(tweets, many=True)
        return Response(serializer.data)
