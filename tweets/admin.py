from django.contrib import admin
from tweets.models import *


# Register your models here.
class TweetAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'owner', 'created', 'public']


admin.site.register(Tweet, TweetAdmin)
