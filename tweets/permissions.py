# -*- coding: utf-8 -*-
from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Кастомное право, позволяющее редактировать объект только владельцам.
    """
    def has_object_permission(self, request, view, obj):
        # Если метод в запросе безопасный, то разрешаем.
        if request.method in permissions.SAFE_METHODS:
            return True
        # Если метод в запросе не безопасный, то разрешаем если только владелец объекта является текующим юзером
        return obj.owner == request.user


class IsTweetPrivate(permissions.BasePermission):
    """
    Custom permission checks access for non-authorized requests to private tweets.
    """
    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated() and not obj.public or obj.public
