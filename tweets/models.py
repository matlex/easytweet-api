from __future__ import unicode_literals
from django.db import models


class Tweet(models.Model):
    title = models.CharField(max_length=100, blank=True, default='')
    text = models.TextField(max_length=500)
    owner = models.ForeignKey('auth.User', related_name='tweets')
    created = models.DateTimeField(auto_now_add=True)
    public = models.BooleanField(default=True)

