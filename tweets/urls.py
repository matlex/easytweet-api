from tweets import views
from rest_framework.routers import DefaultRouter

# Router for tweets viewset.
router = DefaultRouter()
router.register(r'tweets', views.TweetViewSet)
